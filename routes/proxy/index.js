var proxy = module.exports;
var url = require('url');
var http = require('http');
var https = require('https');


function _extend(origin, add) {
    // Don't do anything if add isn't an object
    if (add === null || typeof add !== 'object') return origin;

    var keys = Object.keys(add);
    var i = keys.length;
    while (i--) {
        origin[keys[i]] = add[keys[i]];
    }
    return origin;
}


function _tryBlock(block) {
    var error;
    try {
        block();
    } catch (e) {
        error = e;
    }
    return error;
}

//设置外部请求参数
function setupOutgoingRequest(req,target) {
    var opt = {
        headers:{}
    };
    //设置请求方式
    opt.method = req.method;
    //设置端口
    opt.port = target.port ||
        (target.protocol=="https:" ? 443 : 80);
    //设置path
    opt.path = target.path;
    //设置头部
    _extend(opt.headers,req.headers);
    //改变host和refer;
    _extend(opt.headers,{
        host:target.host,
        referer:target.href
    });
    opt.hostname = target.hostname;
    return opt;
}
//代理访问
proxy.agent = function(req,res,opt,buf,num){
    if(!num)num=1;
    if(num>5){
        res.end("Too many 302 attempt");
    }
    //如果是post则,定义数据缓冲区
    if(req.method=="POST"){
        var buffer = buf?buf:new Buffer(1024*200);
        req.pipe();
    }



    //解析目标地址
    if(_tryBlock(function(){
            opt.target = typeof opt.target == "string"?url.parse(opt.target):opt.target;
        })){
        console.log("解析url出错");
        res.end('invalid target url');
    }
    //res.end(JSON.stringify(setupOutgoingRequest(req,opt.target)));return
    //发出代理请求
    var proxyReq = (opt.target.protocol === 'https:' ? https : http).request(
        setupOutgoingRequest(req,opt.target)
    );
    if(req.method=="POST")
        buffer&&buffer.pipe(proxyReq);
    else
    {
        proxyReq.end();
    }

    proxyReq.on('error',function (e) {
        console.log('proxy err---------------\n',e.stack);
        res.end('Sorry, the proxy goes wrong');
    });
    req.on('aborted', function () {
        proxyReq.abort();
    });

    proxyReq.on("response",function (proxyRes) {
        //接收到了response,分析是否为转跳,转跳则需重新发起
        if(proxyRes.headers['location']&& /^201|30(1|2|7|8)$/.test(proxyRes.statusCode)){
            console.log("redirect Number"+num+"):-----------\n"+proxyRes.headers['location']);
            proxy.agent(req,res,_extend(opt,{
                target:url.parse(proxyRes.headers['location'],true)
            }),buf,num+1);
            return;
        }
        //http降级
        if (req.httpVersion === '1.0') {
            delete proxyRes.headers['transfer-encoding'];
            proxyRes.headers.connection = req.headers.connection || 'close';
        } else if (!proxyRes.headers.connection) {
            proxyRes.headers.connection = req.headers.connection || 'keep-alive';
        }
        //设置返回客户端的头部
        Object.keys(proxyRes.headers).forEach(function(key) {
            try{
               if(!/content-disposition/i.test(key)) res.setHeader(String(key).trim(), proxyRes.headers[key]);
            }catch (e){
                console.log("------error header---------\n",key,proxyRes.headers[key]);
            }
        });
        //设置返回状态
        if(proxyRes.statusMessage) {
            res.writeHead(proxyRes.statusCode, proxyRes.statusMessage);
        } else {
            res.writeHead(proxyRes.statusCode);
        }
        //内容输出到客户端
        proxyRes.pipe(res);
        buffer = null;
    })
};